# konstantų, naudojamų šifruojant SHA-512 inicializavimas
H = [
    0x6a09e667f3bcc908, 0xbb67ae8584caa73b,
    0x3c6ef372fe94f82b, 0xa54ff53a5f1d36f1,
    0x510e527fade682d1, 0x9b05688c2b3e6c1f,
    0x1f83d9abfb41bd6b, 0x5be0cd19137e2179
]

K = [
    0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
    0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
    0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
    0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
    0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
    0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
    0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
    0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
    0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
    0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
    0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
    0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
    0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
    0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
    0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
    0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
    0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
    0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
    0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
    0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
]

import argparse


def readtobinary (file):

    # funkcija, skirta failo skaitymui ir reiksmes issaugojimui bitais

    binary_data = file.read()
    binary_representation = ''.join(format(byte, '08b') for byte in binary_data)
    return binary_representation

def paddedmessage (binary_representation):

    # kaip nurodoma SHS (Secure Hash Standard) dokumentacijoje, formulė l + 1 + k = 896mod1024 yra 'padded message'
    # persitvarkiau formulę, kad l + 1 + k + 128 = padded_message, kur l yra binary_representation, k yra padding_zeroes, 1 yra delimiter bit, 128 yra length_padding+length_binary_representation
    # padded message turi būti tokio ilgio, jog jo modulis būtų lygus nuliui (1024, 2048, tt.)
    # galutinė formulė gavosi padding_zeroes = 1024 * multiplier - len(binary_representation) - 129, kur multiplier = (len(binary_representation) // 1024) + 1

    # multiplier sukurtas tam, kad kai bitų žinutėje yra daugiau nei 1024, būtų galima be vargo suskaičiuoti blokus (1 blokas - 1024 bitai)
    multiplier = len(binary_representation) // 1024 + 1

    # skaičiuojamas k (padding_zeroes)
    padding_zeroes = 1024 * multiplier - len(binary_representation) - 129

    # bitų skaičius žinutėje, išreikštas dvejetaine forma, kuris pridedamas prie padded_message galo, kuris užima length_binary_representation iš 128ių bitų dvejetainėje sistemoje
    length_binary_representation = bin(len(binary_representation))[2:]

    # likusių 128 bitų bloko dalis, sudaryta tik iš nulių. Apskaičiuojama, kiek nulių yra prieš length_binary_representation, kuriuos abu sudėjus turi gautis 128 bitai
    length_padding = int(128 - len(length_binary_representation))

    # sudedami visi nuliai (k ir nulių dalis iš 128 bitų bloko pridedamo gale)
    padding_zeroes += length_padding

    # sudedamos visos dalys, jog gautumėme 'padded message'
    padded_message = binary_representation + "1" + "0" * padding_zeroes + length_binary_representation

    # rezultatas - kintamasis, kuris yra skaiciaus 1024 daliklis ir yra paruostas zinutes skaidymui blokais bei maisos algoritmui pagal reikalavimus
    return padded_message

def parsedmessage(padded_message):

    # funkcija, skirta zinutes isskaidymui blokais po 1024 bitus

    blocks_count = len(padded_message) // 1024
    parsedmessage = [padded_message[i * 1024: (i + 1) * 1024] for i in range(blocks_count)]
    
    return parsedmessage

def expandmessage(parsed_message, H):

    # funkcija, skirta kiekvieno bloko ispletimui i 80 zodziu
        
    # iteracija per kiekviena parsed_message bloka, ispleciant zinute i 80*64 bitu zodzius

    for block in parsed_message:
        block_expanded = []

        # touple unpacking, specifinis Pythono priskyrimo budas, kai elementam, kuriu kiekis yra toks pat kaip saraso elementu kiekis, priskiriamos saraso elementu reiksmes
        a, b, c, d, e, f, g, h = H

        # ispletimas i pradinius 16*64 bitu zodzius, kadangi bloke (1024'iuose bituose) tiek ir telpa
        for i in range(0, 1024, 64):
            
            # pasirinktas int binary tipas, jog butu galima atlikti bitwise operacijas
            word = int(block[i:i+64], 2)
            block_expanded.append(word)

        updatevariables(block_expanded, a, b, c, d, e, f, g, h)

def updatevariables(block_expanded, a, b, c, d, e, f, g, h):

    # funkcija, skirta zinutes ispletimui ir likusiu 64 zodziu sukurimui pagal maisos algoritmo aprasa

    for i in range(16, 80):

    # sukuriami like 64 zodziai, kad sudarytu 80 zodziu
        generateword(i, block_expanded)
        
    for i in range (80):

    # ciklas, skirtas laikintiesiems kintamiesiems naujinti (priskirti naujas reiksmes naudojantis formulemis apibreztomis SHA-512 vadove)

        T1 = h + Sigma1(e) + Ch(e, f, g) + K[i] + block_expanded[i]
        T2 = Sigma0(a) + Maj(a, b, c)
        h = g
        g = f
        f = e
        e = (d + T1) % (2 ** 64)
        d = c
        c = b
        b = a
        a = (T1 + T2) % (2 ** 64)

    for i in range (len(H)):

        H[i] = (H[i] + [a, b, c, d, e, f, g, h][i]) % (2 ** 64)
 
def generateword(i, block_expanded):

    # funkcija, skirta zodziu 16-80 kurimui is turimu 16 zodziu

    word = (sigma1(block_expanded[i-2]) + block_expanded[i-7] + sigma0(block_expanded[i-15]) + block_expanded[i-16]) % (2 ** 64)

    block_expanded.append(word)

    # skaiciavimu funkcijos

def sigma0(word):

    rotr1 = (word >> 1) | (word << (64 - 1))
    rotr2 = (word >> 8) | (word << (64 - 8))
    shr1 = word >> 7

    xor1 = rotr1 ^ rotr2
    xor2 = xor1 ^ shr1

    return xor2

def sigma1(word):

    rotr1 = (word >> 19) | (word << (64 - 19))
    rotr2 = (word >> 61) | (word << (64 - 61))
    shr3 = word >> 6

    xor1 = rotr1 ^ rotr2
    xor2 = xor1 ^ shr3

    return xor2

def Sigma0(word):

    rotr1 = (word >> 28) | (word << (64 - 28))
    rotr2 = (word >> 34) | (word << (64 - 34))
    rotr3 = (word >> 39) | (word << (64 - 39))

    xor1 = rotr1 ^ rotr2
    xor2 = xor1 ^ rotr3

    return xor2

def Sigma1(word):

    rotr1 = (word >> 14) | (word << (64 - 14))
    rotr2 = (word >> 18) | (word << (64 - 18))
    rotr3 = (word >> 41) | (word << (64 - 41))

    xor1 = rotr1 ^ rotr2
    xor2 = xor1 ^ rotr3

    return xor2

def Ch(x, y, z):

    result = (x & y) ^ (~x & z)

    return result

def Maj(x, y, z):

    result = (x & y) ^ (x & z) ^ (y & z)

    return result

def main():
    
    # pythono modulis skirtas komandines eilutes parametrams skaityti
    # nuskaitomas failo pavadinimas is komandines eilutes ivesties ir objektas atidaromas nurodomu budu, siuo atveju 'rb' (baitais)
    parser = argparse.ArgumentParser(description='SHA-512 maišos algoritmas')
    parser.add_argument('filename', type=argparse.FileType('rb'), help='Pateikite failo pavadinimą')
    args = parser.parse_args()

    # kvieciama skaitymo bitais funkcija
    binary_representation = readtobinary(args.filename)

    # kvieciama funkcija, skirta isitikinimui, jog pakanka ivestis bitais yra skaiciaus 1024 daliklis, jei ne, pridedami bitai pagal SHA-512 reikalavimus
    padded_message = paddedmessage(binary_representation)

    # kvieciama zinutes skaidymo blokais funkcija
    parsed_message = parsedmessage(padded_message)

    # funkcija, skirta paciam SHA-512 algoritmui, naudojantis paruosta zinute. 
    expandmessage(parsed_message, H)


    final_hash = ''.join(format(h, '016x') for h in H)

    print(final_hash)

if __name__ == "__main__":
    main()
